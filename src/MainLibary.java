
public class MainLibary {
	public static void main(String[] args){
		LibraryRoom lib = new LibraryRoom();
		Student jow = new Student("Jow","123456789");
		Instructor java = new Instructor("Java","ComSci");
		Student sarah = new Student("Sarah","000000000");

		//limit borrow 7 day
		Book harry1 = new Book("Harry Potter 1",7);
		Book harry2 = new Book("Harry Potter 2",7);
		Book harry3 = new Book("Harry Potter 3",7);
		Book harry4 = new Book("Harry Potter 4",7);
		Book harry5 = new Book("Harry Potter 5",7);
		Book harry6 = new Book("Harry Potter 6",7);
		Book harry7 = new Book("Harry Potter 7",7);
		Book abc = new Book("ABC",7);
		Book math = new Book("Math",7);
		Book biology = new Book("Biology",7);
		Book com = new Book("Hello wold",7);
		Book thai = new Book("Thai",7);

		ReferancesBook noteBook = new ReferancesBook("notebook lenovo");
		ReferancesBook  ref= new ReferancesBook("reference Book");		

		//create Libary add book and add Reader
		lib.addReferanceBook(ref);
		lib.addReferanceBook(noteBook);

		
		lib.addBook(harry1);
		lib.addBook(harry2);
		lib.addBook(harry3);
		lib.addBook(harry4);
		lib.addBook(harry5);
		lib.addBook(harry6);
		lib.addBook(harry7);
		lib.addBook(math);
		lib.addBook(abc);
		lib.addBook(biology);
		lib.addBook(com);
		lib.addBook(thai);

		
		lib.addReader(jow);
		lib.addReader(java);
		lib.addReader(sarah);
		// test get book

		System.out.println("Libary get all ReferancesBook :");
		System.out.println(lib.getAllrefBooks());
		System.out.println("Libary get all Book :");
		System.out.println(lib.getAllBooks());
		System.out.println("Libary get all Reader :");
		System.out.println(lib.getAllReader());
		
		
		System.out.println("Libary borrow ReferancesBook : name "+sarah.getName()+" ,"+harry1.getName()+", date 1/1/2557");
		System.out.println(lib.borrowRefBook(sarah, ref, 1, 1, 2557));

		System.out.println("Libary borrow Book : name "+jow.getName()+" ,"+harry1.getName()+", date 1/1/2557");
		System.out.println("Libary borrow Book : name "+jow.getName()+" ,"+harry2.getName()+", date 1/1/2557");
		System.out.println("Libary borrow Book : name "+jow.getName()+" ,"+harry3.getName()+", date 1/1/2557");
		System.out.println("Libary borrow Book : name "+jow.getName()+" ,"+harry4.getName()+", date 1/1/2557");

		lib.borrowBook(jow, harry1,1,1,2557);
		lib.borrowBook(jow, harry2,1,1,2557);
		lib.borrowBook(jow, harry3,1,1,2557);
		lib.borrowBook(jow, harry4,1,1,2557);//jow have limit 3 book can not add
		
        System.out.println("\nJow get Books :" + jow.getBorrowBook()+"\n");
        

		System.out.println("Libary borrow Book : name "+java.getName()+" ,"+math.getName()+", date 1/1/2557");
		System.out.println("Libary borrow Book : name "+java.getName()+" ,"+abc.getName()+", date 1/1/2557");
		System.out.println("Libary borrow Book : name "+java.getName()+" ,"+com.getName()+", date 1/1/2557");
		System.out.println("Libary borrow Book : name "+java.getName()+" ,"+biology.getName()+", date 1/1/2557");


		lib.borrowBook(java, math,1,1,2557);
		lib.borrowBook(java, com,1,1,2557);
		lib.borrowBook(java, abc,1,1,2557);
		lib.borrowBook(java, biology,1,1,2557);
		
        System.out.println("\nJava get Books :" + java.getBorrowBook()+"\n");
        
		System.out.println("Libary return Book : name Jow ,Harry Potter 1, date 8/1/2557");
		System.out.println(lib.returnBook(jow,harry1,8,1,2557));
		System.out.println("Libary return Book : name Jow ,Harry Potter 2, date 30/1/2557");
		System.out.println(lib.returnBook(jow,harry2,30,1,2557));
		
		System.out.println("\nJow get Books :" + jow.getBorrowBook()+"\n");

	}
	
}
