
public class Instructor extends Reader {
	private String department;
	private int limitBook;
	public Instructor(String name, String department) {
		// TODO Auto-generated constructor stub
		super(name);
		this.department = department;
		limitBook = 0;
	}
	public String getDepartment(){
		return department;
	}
	public String toString(){
		return "Department "+department+" name :"+super.getName();
	}
	public void addBook(Book book) {
		if(limitBook < 7){
			limitBook++;
		super.addBook(book);
		}
		else{
			book.addReaderNull();
		}
	}

}
