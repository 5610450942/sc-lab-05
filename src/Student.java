
public class Student extends Reader {
	private String id;
	private int limitBook;
	public Student(String name, String id) {
		// TODO Auto-generated constructor stub
		super(name);
		this.id = id;
		limitBook = 0;
	}
	public String getID(){
		return id;
	}
	public String toString(){
		return "Id "+id+" name :"+super.getName();
	}
	public void addBook(Book book) {
		if(limitBook < 3){
			limitBook++;
		super.addBook(book);
		}
		else{
			book.addReaderNull();
		}
	}

}
