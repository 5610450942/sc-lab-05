import java.util.ArrayList;


public class LibraryRoom {
 private ArrayList<Book> books = new ArrayList<Book>() ;
 private ArrayList<ReferancesBook> referancesBooks = new ArrayList<ReferancesBook>();
 private ArrayList<Reader> readers =new ArrayList<Reader>();
 public void addBook(Book book){
	 books.add(book);
 }
 public void addReferanceBook(ReferancesBook book ){
	 referancesBooks.add(book);
 }
 public void borrowBook(Reader reader,Book book,int date,int mount,int year){
	 book.addReader(reader);
	 reader.addBook(book);
	 book.setTimeBorrow(date, mount, year);
 }
 public String borrowRefBook(Reader reader,ReferancesBook book,int date,int mount,int year){
	 return "can not borrow ! ! !";
 }
 public String getAllBooks(){
		String strBook = "";
		for(Book b:books){
			strBook += b.getName()+"\n";
		}
		return strBook;
	}
 public String getAllrefBooks(){
		String strBook = "";
		for(ReferancesBook b:referancesBooks){
			strBook += b.getName()+"\n";
		}
		return strBook;
	}
public String returnBook(Reader a, Book book, int d, int m, int y) {
	// TODO Auto-generated method stub
	a.removeBook(book);
	book.addReaderNull();
	int day = book.checkTimeBorrow(d,m,y);
	return "slow return :"+day+" day";
}
public void addReader(Reader r) {
	// TODO Auto-generated method stub
	readers.add(r);
}
public String getAllReader() {
	String strReader = "";
	for(Reader r:readers){
		strReader += r.getName()+" :"+r.getClass()+"\n";
	}
	return strReader;
}
}
